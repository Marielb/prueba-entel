const selectBrand = document.getElementById('marca')
selectBrand.addEventListener('change', handleBrandChange)

function handleBrandChange(e) {
    const brand = e.target.value

    let models = []
    const selectModel = document.getElementById("modelo")

    selectModel.innerHTML=''

    if (brand === 'toyota') {
        models = ['Hilux', 'Sumo', 'Otro']
    }
    if (brand === 'chevrolet'){
        models = ['Aveo', 'Onix Turbo', 'Otro']
    }
    if (brand === 'bmw'){
        models = ['iX', 'X6 M', 'Otro']
    }
    models.map(model => {
        selectModel.insertAdjacentHTML('beforeend', `
        <option value="${model}">${model}</option>
      `)
    })
}

function get_data(e) {
    const nombre = document.getElementById('nombre').value
    const rut = document.getElementById('rut').value
    const patente = document.getElementById('patente').value
    const marca = document.getElementById('marca').value
    const modelo = document.getElementById('modelo').value
    const color = document.getElementById('color').value
    return {nombre, rut, patente, marca, modelo, color}
}

// --------- GUARDAMOS NUESTRO FORMULARIO E INPUTS EN CONSTANTES ---------------
const $formulario = document.getElementById("formulario");
const $inputs = document.querySelectorAll("#formulario input")
const $select = document.querySelectorAll("#formulario select")

// --------- OBJETO CON NUESTRAS EXPRESIONES REGULARES ---------------
const expresiones = {
    nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // AQUI ACEPTARA LETRAS CON O SIN ACENTO Y ESPACIOS
    rut: /^[kK0-9\-]{9,10}$/, // AQUI LE ESTAMOS DICIENDO QUE EN EL CAMPO USUARIO ACEPTE LA K EN MINUSCULA Y MAYUSCULA, NÚMEROS DEL 0 HASTA EL 9, GUIONES MEDIO Y UNA CANTIDAD MINIMA DE 9 CARACTERES Y MAXIMA DE 10 CARACTERES
    patente: /^.{6}$/, // SÓLO ACEPTARA 6 DIGITOS
}


// -------------- OBJETO CON NUESTROS CAMPOS ----------------------
const campos = {
    nombre: false,
    rut: false,
    patente: false,
    marca: false,
    modelo: false,
    color: false,
}


// --------- SWITCH PARA SELECCIONAR EL INPUT DONDE ÉSTE HACIENDO FOCO EL USUARIO  ---------------
const validarFormulario = (e) => {
 
    switch(e.target.name) {
        case "nombre":
            validarCampo(expresiones.nombre, e.target, "nombre");
        break;
        case "rut":
            validarCampo(expresiones.rut, e.target, "rut");
        break;
        case "patente":
            validarCampo(expresiones.patente, e.target, "patente");
        break;
        case "marca":
             if (e.target.value.length < 1) {
                campos.marca = false;
             } else {
                campos.marca = true;
             }
        break;
        case "modelo":
            if (e.target.value.length < 1) {
                campos.modelo = false;
             } else {
                campos.modelo = true;
             }
        break;
        case "color":
            if (e.target.value.length < 1) {
                campos.color = false;
             } else {
                campos.color = true;
             }
        break;
    }
}


// -------------- VALIDAMOS NUESTROS INPUTS ------------------------
const validarCampo = (expresion, input, campo) => {
    if (expresion.test(input.value)){
        document.getElementById(`grupo__${campo}`).classList.remove("formulario__grupo-incorrecto");
        document.getElementById(`grupo__${campo}`).classList.add("formulario__grupo-correcto");
        document.querySelector(`#grupo__${campo} i`).classList.remove("fa-times-circle");
        document.querySelector(`#grupo__${campo} i`).classList.add("fa-check-circle");
        document.querySelector(`#grupo__${campo} .formulario__input-error`).classList.remove("formulario__input-error-activo");
        campos[campo] = true;
        console.log("Funciona");
    } else {
           document.getElementById(`grupo__${campo}`).classList.add("formulario__grupo-incorrecto");
           document.getElementById(`grupo__${campo}`).classList.remove("formulario__grupo-correcto");
           document.querySelector(`#grupo__${campo} i`).classList.add("fa-times-circle");
           document.querySelector(`#grupo__${campo} i`).classList.remove("fa-check-circle");
           document.querySelector(`#grupo__${campo} .formulario__input-error`).classList.add("formulario__input-error-activo");
           campos[campo] = false;
           console.log("Funciona");
        }
}

// --------- CAPTURAMOS CADA VEZ QUE EL USUARIO PRESIONA UNA TECLA ---------------
$inputs.forEach((input) => {
    input.addEventListener("keyup", validarFormulario);
    input.addEventListener("blur", validarFormulario);
});

$select.forEach((select) => {
    select.addEventListener("keyup", validarFormulario);
    select.addEventListener("blur", validarFormulario);
});

// --------- VALIDAMOS TODO NUESTRO FORMULARIO ---------------
$formulario.addEventListener("submit", (e) => {
    e.preventDefault();

    if(campos.nombre && campos.rut && campos.patente && campos.marca && campos.modelo && campos.color) {
        // ------------------ RECUPERAMOS LOS DATOS -----------------------
        let data = JSON.parse(window.localStorage.getItem("campos")) || [];

        window.localStorage.setItem("campos", JSON.stringify([...data, get_data()]));

        document.getElementById("formulario__mensaje-exito").classList.add("formulario__mensaje-exito-activo");
        setTimeout(() => {
            document.getElementById("formulario__mensaje-exito").classList.remove("formulario__mensaje-exito-activo");            
        }, 3000);
        
        document.querySelectorAll(".formulario__grupo--correcto").forEach ((icono) => {
            icono.classList.remove("formulario__grupo--correcto");
        });
        
        setTimeout(() => {
            location.reload();
        }, 5000);

    } else {
        document.getElementById("formulario__mensaje").classList.add("formulario__mensaje-activo");
    }
});




